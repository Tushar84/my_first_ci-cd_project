import requests

usr = "test"
pwd = "test123"
url = "https://kochdev.service-now.com/api/now/attachment/file/"

headers = {
    "Accept": "application/json",
    "Content-Type": "image/png",
}

params = {
    "table_name": "incident",
    "table_sys_id": "ca62ef191b62d198c250edf1b24bcb39",
    "file_name": "test1.png",
}

with open("test/test1.png", "rb") as file_data:
    data_file = file_data.read()

response = requests.post(
    url, params=params, headers=headers, data=data_file, auth=(usr, pwd)
)

print(response.status_code)
